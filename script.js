console.log("task 1");

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];


let [...rest] = clients1;
let [...rest1] = clients2;
let clients = [];




function addItemToArray(arr) {
    arr.forEach(item=> {
        clients.push(item)
    })
}
addItemToArray(rest1);
addItemToArray(rest);
let filteredClients = [...new Set(clients)];
console.log(filteredClients);


console.log("########################################################");
console.log("task 2");
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const charactersShortInfo = characters.map((
    function ({name,lastName,age}){
        return {name, lastName, age};
    }
))

console.log(charactersShortInfo);


console.log("########################################################");
console.log("task 3");
const user1 = {
    name: "John",
    years: 30
};

let  {name, years: age, isAdmin = false} = user1;

console.log(name, age, isAdmin);
console.log("########################################################");
console.log("task 4");
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}


let satoshiSan = Object.assign(satoshi2018,satoshi2019,satoshi2020);
console.log(satoshiSan);
//
// let arr = satoshi2018.toString().split('satoshi');
// console.log(arr)
//
// console.log(satoshi2018.toString() === satoshi2019.toString());
// function checkObjectData(data1, data2) {
//     if (data1.toString() === data2.toString()){
//         console.log("true");
//     }
//     for (let [key2, value2] of Object.entries(data2)) {
//         if (!satoshiSan.hasOwnProperty(key2)) {
//             satoshiSan[key2] = value2;
//         }
//     }
//     console.log(satoshiSan);
//
// }
//
// checkObjectData(satoshiSan, satoshi2018);



console.log("########################################################");
console.log("task 5");

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

let [...restBooks] = books;

const books2 = [...restBooks, bookToAdd ];
console.log(books2);


console.log("########################################################");
console.log("task 6");

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const vitalka = {};

for (let [employeeKey, employeeValue] of Object.entries(employee)) {
    vitalka[employeeKey] = employeeValue;

}
vitalka.salary = 9908;
vitalka.age = 40;

console.log(vitalka);


console.log("########################################################");
console.log("task 7");


const array = ['value', () => 'showValue'];


let [value, showValue] = array;
// Допишіть код тут

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'
